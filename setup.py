import setuptools

setuptools.setup(
    name="preprocessing_package",
    version="0.0.1",
    author="Alric SANS",
    author_email="alric@example.com",
    description="A small example package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)